#!/bin/bash

_shell_files(){
  if [[ -z "$shell_files" ]]; then
    # shellcheck disable=SC2035
    shell_files="$(find * -name "*sh")"
    export shell_files
  fi
  echo "$shell_files"
}


# shellcheck disable=SC2034,SC2155
qa_shfmt(){
  _header "QA: shfmt"

  local report_name=shfmt
  # shellcheck disable=SC2046,SC2086
  report_cmd(){
    _shell_files | xargs shfmt -l ${SHFMT_SHELL:+-ln $SHFMT_SHELL} 3>&2 2>&1 1>&3-
  }
  local report_inraw=true
  local report_etl="$(cat <<-'REPORT'
		split("\n")|map(split(":")|select(length > 0)|{
		type: "issue",
		fingerprint: join(":")|@base64,
		check_name: "shfmt",
		location: {path: .[0], lines: {begin: .[1]}},
		description: .[3:]|join(":")[1:]
		})
	REPORT
  )"

  _qa_report

  error=$(cat /tmp/qaxit 2>/dev/null)
  return "${error:-0}"
}


qa_shfmt_style(){
  _header "QA: style"
  # shellcheck disable=SC2046,SC2086
  _shell_files | xargs shfmt -d -l ${SHFMT_STYLE:--i 2 -ci} ${SHFMT_SHELL:+-ln $SHFMT_SHELL}
}


# shellcheck disable=SC2034,SC2155
qa_shellcheck(){
  _header "QA: shellcheck"

  local report_name=shellcheck
  # shellcheck disable=SC2046,SC2086
  report_cmd(){
    shellcheck ${SHELLCHECK_SHELL:+-s $SHELLCHECK_SHELL} -f json $(_shell_files)
  }
  local report_etl="$(cat <<-'REPORT'
	.[0] | map([{
	  type: "issue",
	  fingerprint: "\(.code):\(.file):\(.line):\(.column)"|@base64,
	  check_name: "shellcheck",
	  description: "[SC\(.code)][\(.level)]: \(.message)",
	  location: {path: .file, lines: {begin: .line, end: .endLine}},
	  severity: .level
	  }]) | add
	REPORT
  )"

  _qa_report | jq -r '.[] | "\(.file):\(.line):\(.column): \(.level): \(.message) [SC\(.code)]"'

  error=$(cat /tmp/qaxit 2>/dev/null)
  return "${error:-0}"
}


qa_shell_intel() {
  _header "SHFMT $(shfmt --version)"
  shfmt --help

  _header "SHELLCHECK"
  shellcheck --version
  shellcheck -?
}
