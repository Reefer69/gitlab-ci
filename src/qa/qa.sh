#@IgnoreInspection BashAddShebang
# shellcheck shell=ksh
QA_REPORT_DIR="${CI_PROJECT_DIR}"

# shellcheck disable=SC2154
_qa_report(){
  report="${QA_REPORT_DIR}/qa_${qa_slug:+${qa_slug}_}${report_name}.json"
  mkdir -p "$(dirname "$report")"

  # GRMBL you try fetching the output & the report & the exit status & passing it on SOMEHOW *without a tempfile*
  [[ ! -f /tmp/qaxit ]] || rm /tmp/qaxit

  { report_cmd || { echo "$?" >/tmp/qaxit ;} ;} | tee >(
    jq -cs${report_inraw:+R} "$report_etl" >"$report"
  )

  _qa_report_out "$report"

  error=$(cat /tmp/qaxit 2>/dev/null)
  return "${error:-0}"
}


_qa_report_out(){
  if echo "$VERBOSE" | grep -q -e "all" -e "lint"; then
    _header "QA: $1 report"
    jq . "$1" >&2
  fi
}


qa_report_merge(){
  report="$1"
  mkdir -p "$(dirname "$report")"
  shift
  jq -cs '.|add|unique_by(.fingerprint)' "$@" >"$report"

  _qa_report_out "$report"
}


qa(){
  qa_slug="$1"
  error=0
  for profile in shell yaml; do
    echo "$QA" | grep -q -e "all" -e "$profile" || continue
    case $profile in
       shell) mods="shfmt shellcheck" ;;
      python) mods="pylint flake8" ;;
           *) mods="$mod" ;;
    esac
    for mod in $mods; do
      "qa_$mod" || true
#      _header "$mod errored $error ?"
      [[ -s /tmp/qaxit ]] || continue
#      _header "$mod errored $error + $(cat /tmp/qaxit)"
      error=$((error + $(cat /tmp/qaxit)))
  done; done

  _header "Generating QA report with $error errors..."
  report="${QA_REPORT_DIR}/qa_${qa_slug}.json"
  (
    # shellcheck disable=SC2164
    cd "$QA_REPORT_DIR"
    [[ -f "$report" ]] && rm "$report"
    # shellcheck disable=SC2086
    qa_report_merge "$report" qa_${qa_slug}_*.json
    #&& find * -depth 1 -name "qa_${qa_slug}_*.json" -exec rm {} \;
  )

  return "${error:-0}"
}
