#!/bin/bash

_shell_files(){
  if [[ -z "$shell_files" ]]; then
    # shellcheck disable=SC2035
    shell_files="$(find * -name "*sh")"
    export shell_files
  fi
  echo "$shell_files"
}


# shellcheck disable=SC2034,SC2155
qa_pylint(){
  _header "QA: pylint"

  local report_name=pylint
  # shellcheck disable=SC2046,SC2086
  report_cmd(){
    pylint --output-format=json --rcfile=/etc/pylintrc ./*
  }
  local report_etl="$(cat <<-'REPORT'
	.[0] | map([{
	  type: "issue",
    fingerprint: "\(.message-id):\(.path):\(.line):\(.column)"|@base64,
    check_name: "pylint",
    description: "[\(.message-id)][\(.type)][\(.symbol)]: \(.message\)",
    location: {path: .path, lines: {begin: .line}},
    severity: .type
	  }]) | add
	REPORT
  )"

  _qa_report | jq -r '.[] | "\(.path):\(.line):\(.column): \(.type) \(.symbol): \(.message) [\(.message-id)]"'

  error=$(cat /tmp/qaxit 2>/dev/null)
  return "${error:-0}"
}


# shellcheck disable=SC2034,SC2155
qa_flake8(){
  _header "QA: flake8"

  local report_name=flake8
  # shellcheck disable=SC2046,SC2086
  report_cmd(){
    flake8
  }
  local report_etl="$(cat <<-'REPORT'
#	.[0] | map([{
#	  type: "issue",
#	  fingerprint: "\(.code):\(.file):\(.line):\(.column)"|@base64,
#	  check_name: "shellcheck",
#	  description: "[SC\(.code)][\(.level)]: \(.message)",
#	  location: {path: .file, lines: {begin: .line, end: .endLine}},
#	  severity: .level
#	  }]) | add
	REPORT
  )"

  _qa_report

  #| jq -r '.[] | "\(.file):\(.line):\(.column): \(.level): \(.message) [SC\(.code)]"'

  error=$(cat /tmp/qaxit 2>/dev/null)
  return "${error:-0}"
}


qa_python_intel() {
  _header "PYTHON $(python --version)"

  _header "PyLint $(pylint --version)"
  pylint --long-help

  _header "Flake8 $(flake8 --version)"
  flake8 --help

}
