#@IgnoreInspection BashAddShebang
# shellcheck shell=ksh

# shellcheck disable=SC2034,SC2155
qa_yaml() {
  _header "QA: YAML"

  local report_name=yaml
  # shellcheck disable=SC2046,SC2086
  report_cmd() {
    yamllint -s -f parsable -d "$(cat /etc/yamllint/config .yamllint)" .
  }

  local report_inraw=true
  local report_etl="$(
    cat <<-'REPORT'
		split("\n")|map(split(":")|select(length > 0)|{
		  type: "issue",
		  fingerprint: join(":")|@base64,
		  check_name: "yaml",
		  location: {path: .[0], lines: {begin: .[1]}},
		  description: .[3:]|join(":")
		    |sub(" \\[(?<sev>[^\\]]+)\\] (?<desc>.*) \\((?<rule>.*)\\)$";"[\(.rule)][\(.sev)]: \(.desc)"),
		  severity: .[3]|split("[")[1]|split("]")[0]
		})
	REPORT
  )"

  _qa_report

  error=$(cat /tmp/qaxit 2>/dev/null)
  return "${error:-0}"
}


qa_yaml_intel() {
  _header "QA: YAMLlint $(yamllint -v)"
  yamllint --help

  _header "QA: YAML config data"
  cat /etc/yamllint/config .yamllint
}
