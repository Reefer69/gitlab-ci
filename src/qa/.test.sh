#!/bin/bash
set +x
cd /builds
rm -f /tmp/*json
reset

export VERBOSE=shell
export QA=python
#export SHFMT_SHELL=posix

export \
  CI_PROJECT_DIR=/tmp \
  CI_JOB_NAME=test

. src/lib.sh
. src/qa/qa.sh
. src/qa/python.sh
#. src/qa/shell.sh
. src/qa/yaml.sh

###

#set -x
qa python-report

#qa shell-report

#qa_shell

#qa_shfmt

#qa_shellcheck

