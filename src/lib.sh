#@IgnoreInspection BashAddShebang
# shellcheck shell=ksh

#------------------------ include common functions ------------------------#
(echo "$VERBOSE" | grep -q -e "all" -e "shell") && set -x
set -e -o pipefail

PROJECT_TS_RFC3339=$(date -u '+%Y-%m-%dT%TZ')
VERSION_TS=$(date -u '+%y.%j.%H%M%S')
VERSION_TS_DAY=$(date -u '+%Y.%m.%d')
VERSION_TS_MONTH=$(date -u '+%Y.%m')
VERSION_TS_WEEK=$(date -u '+%Y.%V')
[[ -n "$UID" ]] || { UID=$(id -u) && export UID; }
export PROJECT_TS_RFC3339 VERSION_TS VERSION_TS_DAY VERSION_TS_MONTH VERSION_TS_WEEK


_env_get(){
  # B0rked on busybox sh :(
  #val=$1
  #echo "${!val}"

  #res=$(env | sed -E -e "/^$1=/!d" -e 's|^[^=]+=||g')
  # shellcheck disable=SC2086
  eval 'echo "${'$1':-'$2'}"'
}


_env_keys(){
  #nope
  #echo "${!$1*}"
  env|awk -F= "/^$1/{ print \$1 }"
}


_header() {
  # Nicely formatted demarkation line. You're welcome ^^
  cat >&2 <<-TPL


		#------------------------ $1 ------------------------#
		#${2+ $2}

TPL
}


_params_env() {
  # Transforms variable vars into params
  cli_arg=$1
  args=$2
  kwargs=$3

  echo "Building $cli_arg from args:\"${args}\" and kwargs:\"${kwargs}\"" >&2

  # Compile args
  # shellcheck disable=SC2086
  for var in $args; do
    vals="$vals $(_env_get $var) "
  done

  # Compile kwargs
  # shellcheck disable=SC2086
  for var in $kwargs; do
    vars=$(_env_get "$var")
    [[ -n "$vars" ]] || continue
    for key in $vars; do
      val=$(_env_get "$key")
      [[ -n "$val" ]] || continue
      vals="$vals ${key}=${val} "
    done
  done

  _params "$cli_arg" "$vals"
}


_params() {
  # Transforms vals into params
  arg=$1; vals=$2
  # Compile args
  for val in $vals; do
    echo -n "--$arg \"$val\" "
  done
}


_upper() {
  #B0rked on busybox sh :(
  #args="$*"
  #echo "${args^^}"
  echo "$@" | tr '[:lower:]' '[:upper:]'
}


project_intel() {
  _header "ADVANCED SHELL STUFF"
  echo "We uppered: $(_upper allcases)"
  echo "Get env HOME = $HOME = $(_env_get HOME)"

  _header "PROJECT ENVIRONMENT"
  env | sort

  _header "MOUNTS"
  mount

  _header "BUILDS DIR ($CI_BUILDS_DIR)"
  ls -al "$CI_BUILDS_DIR"

  _header "CURRENT DIR ($PWD) == PROJECT DIR ($CI_PROJECT_DIR)"
  ls -al
}
