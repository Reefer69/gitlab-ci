#!/bin/bash
[[ -n "$DEBUG" ]] && set -x

#glinc=
gltpl=src/
. src/lib.sh


loop(){
  cd src/ || exit 1
  find . -name '*.yml' -o -name '*.yaml' | while read -r tpl; do
    dst=../$(dirname "$tpl")
    echo "Generating ${tpl:2}..."
    mkdir -p "$dst"
    generate "$tpl" >"../$tpl"
    git add "$dst"
  done
}


generate() {
  gltpl=$1
  _header "GENERATE-INCLUDED FUNCTIONS" "Generated from src/! Change the scripts & re-generate." 2>&1 | sed -E '1,3{/^[[:space:]]*$/d}'

  # Find all YAML *refs & see if they exist as files
  sed -E '/ \*/!d;s|^[^*]+ \*||g' "$gltpl" | sort -u | (
    while read -r tpl; do
      [[ -s "$tpl" ]] || continue
      anchor=$(echo "$tpl" | sed -E 's|\.[^.]*$||g;s|[/.]|--|g')
      anchors="$anchors -e s|\*${tpl}([[:space:]]*)|*${anchor}\1|g"

      cat <<-YAML

.${anchor}: &${anchor} |-
YAML
      # Remove comments & indent
      #-e '/^[[:space:]]*#([@!]|[[:space:]]*shellcheck)/d' \
      sed -E \
        -e '/^[[:space:]]*(#[^-#].*)?$/d' \
        -e 's/^/  /' "$tpl"
    done

  _header "END OF FUNCTIONS" 2>&1

  if [[ -n "$anchors" ]]; then
    # shellcheck disable=SC2086
    sed -E $anchors "$gltpl"
  else
    cat "$gltpl"
  fi
  )
}


loop
