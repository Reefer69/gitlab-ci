#@IgnoreInspection BashAddShebang
# shellcheck shell=ksh

#------------------------ Kaniko functions ------------------------#
kaniko_intel() {
  _header "KANIKO DIR"
  ls -alR /kaniko

  _header "KANIKO HELP"
  /kaniko/executor --help

  _header "USER RUN"
  ls -alR /run || true

  _header "WHOS ME"
  # shellcheck disable=SC2169
  echo $UID
  id -u
}


kaniko_authn() {
  export DOCKER_CONFIG=/run/user/$UID
  # shellcheck disable=SC2046
  _cri_authn $(_oci_reg_authn)
}


kaniko_build() {
  _header "Kaniko build of $DOCKERFILE"
  tags=$(_oci_tags)
  cat <<-INFO
    Destination tags:
    $tags
INFO

  [[ -n "$OCI_DEBUG" ]] && log_level=debug

  # https://github.com/GoogleContainerTools/kaniko/issues/465
  # $(_cri_args_labels) \
  # shellcheck disable=SC2046
  eval $(cat <<-COMMAND
  exec /kaniko/executor \
    --verbosity "${log_level:-info}" \
    --reproducible --cache true --cache-dir "$OCI_CACHE_DIR" --cache-repo "$REGISTRY_CACHE" \
    --dockerfile "$DOCKERFILE" \
    $(_cri_args_build) \
    $(_params destination "$tags") \
    --context "$CI_PROJECT_DIR"
COMMAND
  )
}
