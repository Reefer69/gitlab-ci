#@IgnoreInspection BashAddShebang
# shellcheck shell=ksh

#------------------------ Buildah functions ------------------------#
buildah_intel() {
  _header "BUILDAH HELP"
  buildah --help

  _header "BUILDAH BUD HELP"
  buildah bud --help
}


buildah_authn() {
  AUTH_JSON=/run/user/$UID/auth.json
  # shellcheck disable=SC2046
  _cri_authn $(_oci_reg_authn)
}


buildah_build() {
  _header "Buildah build of $DOCKERFILE"
  tags=$(_oci_tags)
  cat <<-INFO
    Destination tags:
    $tags
INFO

  #  --isolation=chroot \
  # shellcheck disable=SC2046
  eval $(cat <<-COMMAND
  buildah ${OCI_DEBUG:+--debug} bud \
    --root "${OCI_CACHE_DIR}" \
    --runroot "/run/user/$(id -u)/containers" \
    $(_cri_args_build) \
    $(_cri_args_labels) \
    $(_params tag "$tags") \
    --file "$DOCKERFILE" \
    "${CI_PROJECT_DIR}"
COMMAND
  )

  _header "Pushing tags..."
  echo "AUTH_JSON: $AUTH_JSON"
  for tag in $tags; do
    echo "Pushing ${tag}..."
    buildah ${OCI_DEBUG:+--debug} \
      --root "${OCI_CACHE_DIR}" \
      --runroot "/run/user/$(id -u)/containers" \
      push --authfile "$AUTH_JSON" "$tag"
  done
}
