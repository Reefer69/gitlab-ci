#@IgnoreInspection BashAddShebang
#shellcheck shell=ksh

#------------------------ include OCI functions ------------------------#
export OCI_CACHE_DIR="${CI_PROJECT_DIR}/.build/oci"
if echo "$VERBOSE" | grep -q -e all -e oci; then
  export OCI_DEBUG=yesplz
fi

_oci_reg_authn() {
  for reg_user in CI_REGISTRY_USER $(_env_keys 'REGISTRY_.*_USER'); do
    reg_name=$(echo "$reg_user"|cut -d_ -f2)
    if [[ "$reg_user" == "CI_REGISTRY_USER" ]]; then
      reg_user="$CI_REGISTRY_USER"
      reg_pass="$CI_REGISTRY_PASSWORD"
    else
      reg_user="$(_env_get "$reg_user")"
      reg_pass="$(_env_get "REGISTRY_${reg_name}_PASSWORD")"
    fi

    reg_server="$(_oci_reg "$reg_name")"
    [[ -n "$reg_server" ]] || { echo "No server found for container registry $reg_name!"; continue; }
    [[ "$reg_server" != "DISABLED" ]] || continue
    [[ -n "$reg_pass" ]] || { echo "No password given for container registry $reg_name!'"; continue; }

    echo -n "$reg_server $reg_user $reg_pass "
  done
}


_cri_authn_json() {
  cat <<-JSON
		{"auths": {
JSON
  while [[ $# -gt 0 ]]; do
    reg_server=$1
    reg_user=$2
    reg_pass=$3
    shift; shift; shift
    echo "Registering ${reg_user}@${reg_server}" >&2

    if [[ $# -gt 0 ]]; then
      comma=,
    else
      comma=
    fi
    cat <<-JSON
			"$reg_server": {"auth": "$(echo -n "${reg_user}:${reg_pass}" | base64)" }$comma
JSON

  done
  cat <<-JSON
		}}
JSON
}


_cri_authn() {
  # Generates a Docker/whoever config.json with registry auths
  json="${AUTH_JSON:-${DOCKER_CONFIG:-${HOME}/.docker}/config.json}"
  json_dir="$(dirname "$json")"

  _header "Writing registry authentications to ${json}..."
  # shellcheck disable=SC2174
  [[ -d "$json_dir" ]] || mkdir -pm 700 "$json_dir"
  _cri_authn_json "$@" >"$json"

  if echo "$VERBOSE" | grep -q trace; then
    echo 'Registry authentications result:'
    cat "$json"
  fi
}


_cri_build_params() {
  arg=$1
  var_base=$2
  args="OCI_BUILD_${var_base} $(_env_get "OCI_BUILD_${var_base}_EXTRA")"
  kwargs="OCI_BUILD_${var_base}_ENV OCI_BUILD_${var_base}_ENV_EXTRA"
  _params_env "$arg" "$args" "$kwargs"
}


_cri_args_build() {
  # Compile build args
  _cri_build_params build-arg ARGS
}


_cri_args_labels() {
  # Compile image labels
  # Labels from https://github.com/opencontainers/image-spec/
  # TODO: WTF is ref.name supposedto be anyway???
  prefix=org.opencontainers.image
  for var in authors created documentation description licenses revision source title url version vendor; do
    slug="$(_upper ${var})"
    [[ -z "$(_env_get "OCI_LABEL_${slug}_DISABLED")" ]] || continue

    case $var in
      authors) def_v="$GITLAB_USER_NAME ($GITLAB_USER_EMAIL)" ;;
      created) def_v=$PROJECT_TS_RFC3339 ;;
      revision) def_v="${CI_COMMIT_TAG:-${CI_COMMIT_SHORT_SHA:-$CI_COMMIT_SHA}}" ;;
      source) def_v="${PROJECT_URL:-${CI_PROJECT_URL}}" ;;
      *) unset def_v ;;
    esac

    val=$(_env_get "PRODUCT_${slug}" "$def_v")
    [[ -n "$val" ]] || continue
    echo -n "--label \"${prefix}.${var}=${val}\" "
  done

  _cri_build_params label LABELS
}


_oci_tags() {
  # For each registry determine tags and return as one list
  default_tags="$REGISTRY_TAGS $REGISTRY_TAGS_EXTRA"
  default_tags_env="$REGISTRY_TAGS_ENV"

  for reg in CI_REGISTRY $(_env_keys 'REGISTRY_.*_USER'); do
    reg_name=$(echo "$reg"|cut -d_ -f2)
    reg_server=$(_oci_reg "$reg_name" | cut -d/ -f3)
    [[ -n "$reg_server" && "$reg_server" != "DISABLED" ]] || continue

    if [[ "$reg" == CI_REGISTRY ]]; then
      env_prefix=CI_REGISTRY
      reg_image=${REGISTRY_IMAGE:-$CI_REGISTRY_IMAGE}
    else
      env_prefix=REGISTRY_${reg_name}
      reg_image=$(_env_get "${env_prefix}_IMAGE")
      if [[ -z "$reg_image" ]]; then
        reg_slug=$(_env_get "${env_prefix}_SLUG" "$(_env_get "${env_prefix}_USER")/$CI_PROJECT_NAME")
        reg_image="${reg_server}/${reg_slug}"
      fi
    fi
    reg_tags="$(_env_get "${env_prefix}_TAGS" "$default_tags") $(_env_get "${env_prefix}_TAGS_EXTRA")"
    reg_tags_env="$(_env_get "${env_prefix}_TAGS_ENV" "$default_tags_env") $(_env_get "${env_prefix}_TAGS_ENV_EXTRA")"
    for tag in $reg_tags; do
      echo -n "${reg_image}:$tag "
    done
    for env_tag in $reg_tags_env; do
      env_tag=$(_env_get "$env_tag")
      [[ -n "$env_tag" ]] || continue
      echo -n "${reg_image}:$env_tag "
    done
  done
}


_oci_reg() {
  # Fetch the registry from env/well-known defaults
  reg_name=$1
  if [[ "$reg_name" == "REGISTRY" ]]; then
    reg_server=$CI_REGISTRY
  else
    reg_server="$(_env_get "REGISTRY_${reg_name}")"
  fi
  # Explictly disabled by env
  if echo "$reg_server" | grep -iq -e false -e disable; then
    echo "DISABLED"
    return
  elif [[ -z "$reg_server" ]]; then
    case $reg_name in
      DOCKER) reg_server=index.docker.io ;;
      GITLAB) reg_server=registry.gitlab.com ;;
      QUAY) reg_server=quay.io ;;
      GOOGLE) reg_server=eu.gcr.io ;;
    esac
  fi

  echo $reg_server
}


_oci_envkey() {
  reg_server=$1
  case $reg_server in
    index.docker.io) reg_name=DOCKER ;;
    registry.gitlab.com) reg_name=GITLAB ;;
    quay.io) reg_name=QUAY ;;
    eu.gcr.io) reg_name=GOOGLE ;;
    gcr.io) reg_name=GOOGLE ;;
  esac

  echo $reg_name
}
