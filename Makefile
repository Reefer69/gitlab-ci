include lib.mk

.PHONY: tpl test-labels

tpl:
	exec src/generate.sh


test:
	. src/lib.sh
	. src/oci/oci.sh
	. src/oci/build-img.sh
	. src/oci/build-kaniko.sh

	TESTVAR=hok
	OOKVAR=kat

	OCI_BUILD_ARGS='kal=bal kol=bol lala'
	OCI_BUILD_ARGS_ENV="TESTVAR OOKVAR"
	_cri_args_build

	_cri_args_labels


.PHONY: qa

qa:
	podman run --rm -it \
		--volume /home/theloeki/WS/OSS/salt-formulae:/src \
		--volume /home/theloeki/WS/OSS/gitlab-ci:/builds \
	registry.gitlab.com/ors-it-oss/oci:qa-python /bin/bash -c /builds/src/qa/.test.sh
